package Miner

import akka.actor._

class LocalBoss extends Boss{

	def receive = {
		case connect() => {
			initialize()
			sender ! connect()
		}

		case setParams(numZeroes: Int, m_ID: String) => {
			startMiners(numZeroes, m_ID)
		}

		case minerReport(m_WID: String, seq: String) => {
			updateMinerReport(m_WID, seq)
		}
	}
}
