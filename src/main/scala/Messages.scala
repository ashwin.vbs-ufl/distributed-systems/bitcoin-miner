package Miner

import akka.actor._

case class minedHash(output: String)

case class kickoff(num: Int, m_UID: String, m_WID: String, seqStart: String, admin: ActorRef)

case class minerReport(m_WID: String, seq: String)

case class connectIP(address: String)

case class setParams(numZeroes: Int, ID: String)

case class connect()

case class bossReport(bossID: String, workerStats: Map[String, String])

case class initlocal(m_UID: String, num: Int, sys: ActorSystem)
