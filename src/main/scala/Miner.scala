package Miner

import java.security.MessageDigest
import akka.actor._
import akka.remote._

//~ case class CustomResizer() extends Resizer {
	//~ var size: Int
	//~ var sizeChanged: Boolean
	//~ def isTimeForResize(messageCounter: Long): Boolean {
		//~ return sizeChanged
	//~ }
	//~ def resize(currentRoutees: IndexedSeq[Routee]): Int {
		//~ return size
	//~ }
//~ }

class Miner extends Actor{
	var zeroes: Int = 0
	var queuedHashes: String = ""
	var numHashes: Int = 0;
	var SeqID: StringGenerator = null
	var zeroString: String = ""
	var admin: ActorRef = null

	var WID: String = ""
	var minerReportFreq: Int = 100
	var hashReportFreq: Int = 1


	def checkAndQueue(seed: String, hash: String) = {
		if(zeroes == 0){
			queuedHashes = queuedHashes + String.format("%-40s%s%n", seed, hash);
			numHashes += 1
		}
		else
			if(hash.substring(0, zeroes) == zeroString){
				queuedHashes = queuedHashes + String.format("%-40s%s%n", seed, hash);
				numHashes += 1
			}
		if(numHashes > hashReportFreq){
			admin ! minedHash(queuedHashes)
			queuedHashes = ""
			numHashes = 0
		}

	}

	def hash (seed: String) : String = {
		val md = MessageDigest.getInstance("SHA-256")
		md.update(seed.getBytes("UTF-8"))
		var hash = md.digest().map("%02x".format(_)).mkString
		//println (seed)
		return hash
	}

	def receive = {
		case kickoff(num: Int, m_UID: String, m_WID: String, seqStart: String, m_admin: ActorRef)=> {
			zeroes = num
			admin = m_admin
			WID = m_WID
			SeqID = new StringGenerator(seqStart)
			zeroString = "0000000000000000000000000000000000000000000000000000000000000000".substring(0, num)
			var it: Int = 0
			while(1==1){
				it += 1
				var seq = SeqID.get()
				var seed: String = m_UID + WID + seq
				checkAndQueue(seed, hash(seed))
				if(it > minerReportFreq){
					it = 0
					sender ! minerReport(WID, seq)
				}
			}
		}
	}
}
