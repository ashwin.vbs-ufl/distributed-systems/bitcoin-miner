package Miner

import akka.actor._
import akka.routing._

abstract class Boss extends Actor{
	var numMiners: Int = 5

	var localPool: ActorRef = null
	//var resizer = DefaultResizer(lowerBound = 2, upperBound = 20)	//TODO: replace this with custom Resizer and call optimize with it
	var MinerID: StringGenerator = null

	var minerStatus: Map[String, String] = Map()
	var bossUpdateFreq: Int = 100
	var numStatUpdates = 0
	var UID: String = ""
	var admin: ActorRef = null

	def optimize = {
	}

	def initialize () = {
		localPool =context.actorOf(Props[Miner].withRouter(RoundRobinRouter(nrOfInstances= numMiners)))
		//  localPool = context.actorOf(RoundRobinPool(5, Some(resizer)).props(Props[Miner]),"localPool")
		//~ optimize()
	}

	def startMiners (num: Int, bossID: String) = {
		MinerID = new StringGenerator("!!")
		var it: Int = 0
		admin = sender
		UID = bossID
		for(it <- 1 to numMiners){
			localPool ! kickoff(num, bossID, MinerID.get(), "!!", sender)
		}
	}

	def updateMinerReport(m_WID: String, seq: String) = {
		minerStatus = minerStatus + (m_WID -> seq)
		numStatUpdates += 1
		if(numStatUpdates > bossUpdateFreq){
			numStatUpdates = 0
			admin ! bossReport(UID, minerStatus)
		}
	}
}
