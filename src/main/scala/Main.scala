package Miner

import akka.actor._
import com.typesafe.config._
import java.net._

object Application extends App {
	var cli_arg:String=args(0)
	if(cli_arg.contains('.'))
	{
		val configString: String = """akka {
			loglevel = "INFO"
			actor {
				provider = "akka.remote.RemoteActorRefProvider"
			}
			remote {
				enabled-transports = ["akka.remote.netty.tcp"]
				netty.tcp {
					hostname = "127.0.0.1"
					port = 8081
				}
				log-sent-messages = on
				log-received-messages = on
			}
		}"""
		val system = ActorSystem("remote", ConfigFactory.parseString(configString))
		val rboss = system.actorOf(Props[RemoteBoss],name="RemoteBoss")
		rboss ! connectIP(cli_arg)
	}
	else
	{
		val ipAddress = InetAddress.getLocalHost.getHostAddress
		val configString: String = """akka {
			loglevel = "INFO"
			actor {
				provider = "akka.remote.RemoteActorRefProvider"
			}
			remote {
				enabled-transports = ["akka.remote.netty.tcp"]
				netty.tcp {
					hostname = """" + ipAddress + """"
					port = 8080
				}
				log-sent-messages = on
				log-received-messages = on
			}
		}"""
		var no_of_zeros=cli_arg.toInt
		val system = ActorSystem("admin", ConfigFactory.parseString(configString))
		val admin = system.actorOf(Props[Admin], name="Admin") //.withDeploy(Deploy(scope = RemoteScope(Address("akka.tcp", "admin", ipAddress, 5150)))),name="Admin")
		println("ADMIN actor: ", admin.toString())
		admin ! initlocal("ashwin.vbs", no_of_zeros, system)
	}

}
