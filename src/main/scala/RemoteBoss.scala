package Miner

import akka.actor._

class RemoteBoss extends Boss{

	def receive = {
		case connectIP(address: String) => {
			var url = "akka.tcp://admin@"+address+":8080/user/Admin"
			var remoteAdmin=context.actorSelection(url)
			initialize()
			remoteAdmin ! connect()
		}

		case setParams(numZeroes: Int, m_ID: String) => {
			startMiners(numZeroes, m_ID)
		}

		case minerReport(m_WID: String, seq: String) => {
			updateMinerReport(m_WID, seq)
		}
	}
}
