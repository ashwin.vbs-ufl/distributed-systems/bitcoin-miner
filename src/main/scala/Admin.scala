package Miner

import akka.actor._

class Admin extends Actor{

	var UID: String = ""
	var numZeroes: Int = 0
	var bossID: StringGenerator = new StringGenerator("!!")

	var bossStats: Map[String, Map[String, String]] = Map()

	def receive = {
		case connect() => {
			sender ! setParams(numZeroes, UID + bossID.get())
		}

		case initlocal(m_UID: String, num: Int, system: ActorSystem) => {
			UID = m_UID
			numZeroes = num
			val localboss = system.actorOf(Props[LocalBoss],name="LocalBoss")
			localboss ! connect()
		}

		case minedHash(output: String) => {
			System.out.printf("%s", output)
		}

		case bossReport(bossID: String, workerStats: Map[String, String]) =>{
			bossStats = bossStats + (bossID -> workerStats)
		}
	}
}
