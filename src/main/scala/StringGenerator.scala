package Miner

class StringGenerator(start: String){

	var currentSeed: Array[Int] = start.reverse.map(_.toInt).toArray

	def get (): String= {
		var nextSeed: Array[Int] = Array[Int]()
		var seedString: String = ""
		var it: Int = 0
		var carry: Int = 1

		while (it < currentSeed.length){
			var temp:Int = carry + currentSeed(it)
			if (temp == 127){
				temp = 33
				carry = 1
			}
			else
				carry = 0
			nextSeed = nextSeed ++ Array[Int](temp)
			seedString += temp.toChar.toString
			it += 1
		}

		if(carry == 1){
			nextSeed = nextSeed ++ Array[Int](33)
			seedString += 33.toChar.toString
		}

		currentSeed = nextSeed
		return seedString.reverse
	}
}
