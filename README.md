Description:

Implements a distributed hash miner which lists all hashes which have a given number of leading zeroes.

Prerequisites:

Sbt and Scala

Instructions to run the program:

* sbt build at the project root.

* sbt “run <parameters>” at the project root will execute the program.

Parameters

1. <numZeroes> - 
will start a server instance with a single local boss instance and few worker instances and start listing mined hashes with atleast numZeroes leading zeroes. The server listens on port 8080 for remote bosses

2. <ipv4 address> - 
Will run a remote boss on that machine and try connecting to the server at ipv4 address at port 8080. The remote boss will subsequently request a batch of hashing jobs and assign them to workers. The mined hashes are passed back to the server.

As the job space is extremely large, the programs will execute ad infinitum. They have to be stopped manually.